# ~/.bashrc: executed by bash(1) for non-login shells.

 #Note: PS1 and umask are already set in /etc/profile. You should not 
 PS1="\[$(tput bold)\]\[\033[38;5;8m\][\[$(tput sgr0)\]\[\033[38;5;39m\]\u@\[\033[38;5;209m\]\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;8m\]:\[$(tput sgr0)\] \@ \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;8m\]]\[$(tput sgr0)\] \[$(tput bold)\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;8m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]"

# need this unless you want different defaults for root.
# PS1='${debian_chroot:+($debian_chroot)}\h:\w\$ '
# umask 022

# You may uncomment the following lines if you want `ls' to be colorized:
# export LS_OPTIONS='--color=auto'
# eval "`dircolors`"
# alias ls='ls $LS_OPTIONS'
# alias ll='ls $LS_OPTIONS -l'
# alias l='ls $LS_OPTIONS -lA'
#
# Some more alias to avoid making mistakes:
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
alias ..='cd ..'
alias ...='cd ../..'
#alias befehle='. /home/robert.baer/Dokumente/Support/alias.txt'
#alias da='. /home/robert.baer/scripte/ping.sh'
alias dnscloud='s 116.202.10.31'
alias dockercloud='s 116.202.26.181'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias h='httping -r'
alias l='ls $LS_OPTIONS -lA'
alias ll='ls $LS_OPTIONS -l'
alias ls='ls $LS_OPTIONS'
alias m='mtr -t --displaymode 1'
alias mtr='mtr -t'
alias mtrs='mtr -r -w -c30'
alias n='nmap'
alias nn='nmap -Pn'
alias phpcloud='s 116.202.26.199'
#alias probleme=' cat /home/robert.baer/Dokumente/Support/Probleme.txt'
#alias pssh='parallel-ssh'
alias rdpcloud='r 49.12.38.148'
#alias remote-burn='remote-shell xfburn 2>/dev/null'
alias s='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l root'
#alias shell='. /home/robert.baer/Dokumente/Support/shellborn.sh'
alias sshserver='s 78.46.161.72'
#alias ssr='ssh -p 125'
alias vpncloud='s 95.216.202.45'

