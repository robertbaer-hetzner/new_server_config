#!/bin/bash

##   Autor: Robert Bär
## Written: 30/06/2020

# Start 

rm ~/.bashrc && cp .bashrc ~/.bashrc && source ~/.bashrc

rm /etc/vim/vimrc && cp vimrc /etc/vim/vimrc
